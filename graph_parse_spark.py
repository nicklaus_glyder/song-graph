from sets import Set
import sys, os, itertools
import numpy as np

# Spark imports
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField
from pyspark.sql.types import StringType, LongType, ArrayType
import pyspark.sql.functions as sqlf

# NLTK for stopwords
import nltk
from nltk.corpus import stopwords
nltk.download("stopwords")

if os.path.isfile(sys.argv[1]):
    db_url = "jdbc:sqlite:{0}".format(os.path.abspath(sys.argv[1]))
    genre_url = "file:{0}".format(os.path.abspath(sys.argv[2]))
    node_url = "file:{0}".format(os.path.abspath(sys.argv[3]))
    edge_url = "file:{0}".format(os.path.abspath(sys.argv[4]))
else:
    err = '''
    Please provide the following files:
    1. sqlite db of song lyric data
    2. csv file of genre information
    3. locaiton to write node list
    4. locaiton to write edge list
    '''
    print(err)
    sys.exit(1)

# Create SPARK Session + Config
conf = SparkConf().setAppName("SQLITE GRAPH GEN")
spark = SparkSession.builder.config(conf=conf).getOrCreate()

# Broadcast so all nodes get the stopword list
STOP_WORDS = spark.sparkContext.broadcast(Set(stopwords.words("english")))

# Use JDBC connector to read tables into DataFrames
lyrics_df = spark.read.format("jdbc")\
    .options(url=db_url, \
             driver="org.sqlite.JDBC", \
             dbtable="lyrics").load()

words_df = spark.read.format("jdbc") \
    .options(url=db_url, \
             driver="org.sqlite.JDBC", \
             dbtable="words").load()

genre_schema = StructType(
    [
        StructField('id', StringType(), False),
        StructField('genre', StringType(), True)
    ]
)

genre_df = spark.read.csv(genre_url, sep='\t', schema=genre_schema)

# Schema we'll end up using to represent a song node
node_schema = StructType(
    [
        StructField('id',        StringType(),             False),
        StructField('node_type', StringType(),             False),
        StructField('words',     ArrayType(StringType()),  True),
        StructField('counts',    ArrayType(LongType()),    True)
    ]
)

word_schema = StructType(
    [
        StructField('id',        StringType(),             False),
        StructField('node_type', StringType(),             False)
    ]
)

# Schema we'll end up using to represent an edge
edge_schema = StructType(
    [
        StructField('src',  StringType(),  False),
        StructField('dst',  StringType(),  False),
        StructField('weight', LongType(), False)
    ]
)

# Pass a row record for distirbuted processing:
#   - Remove stopwords and their counts from the record
#   - Sort the words and counts, retain the top 10
#   - Additionally, we need to ASCII encode the words because the data is not clean
def stop_words(row):
    selector = [x not in STOP_WORDS.value for x in row.words]
    words  = list(itertools.compress(row.words, selector))
    counts = list(itertools.compress(row.counts, selector))
    if len(words) > 0:
        words, counts = zip(*sorted(zip(words, counts), key=lambda (w,c): c, reverse=True))
        words = map(lambda x: x.encode('ascii', 'ignore'), list(words)[:10])
        counts = list(counts)[:10]
        return (row.id, row.node_type, words, counts)
    else:
        return (row.id, row.node_type, [], [])

# Group lyric rows by track_id, s.t. the resulting record
# has a 1:1 list of lyric tokens and counts
song_df = lyrics_df.select(lyrics_df.track_id.alias('id'), \
                           lyrics_df.word, \
                           lyrics_df['count']) \
            .groupBy('id') \
            .agg(sqlf.collect_list('word').alias('words'), \
                 sqlf.collect_list('count').alias('counts')) \
            .withColumn('node_type', sqlf.lit('song'))

# Use distributed map to aggregate lyrics rows
song_df = spark.createDataFrame(song_df.rdd.map(stop_words), node_schema)

# All nodes need to use the same node_schema, so records from
# the word table need to be given null columns
words_df = words_df.select(words_df.word.alias('id')) \
                    .distinct() \
                    .withColumn('node_type', sqlf.lit('word'))

# Just like the lyrics records, we need to ASCII encode the words or
# SPARK will be upset with us since these nodes use the word as an id
encoded_rdd = words_df.rdd.map(lambda row: ( row.id.encode('ascii', 'ignore'), \
                                             row.node_type))

# Turn back into a DF to pass as graph nodes
words_df = spark.createDataFrame(encoded_rdd, word_schema).distinct()

# Only keep songs with a genre
song_df = song_df.join(genre_df, on=['id'])

def edge_map(row):
    edges = []
    for word, count in zip(row.words, row.counts):
        edges.append((row.id, word, 1))
    return edges

# Generate Edges
edge_df = spark.createDataFrame( song_df.rdd.flatMap(edge_map), edge_schema)

# Final node list
node_df = song_df.select('id', 'node_type', 'genre') \
                 .unionAll(words_df.withColumn('genre', sqlf.lit(None)))

node_df.write.format('com.databricks.spark.csv').save(node_url)
edge_df.write.format('com.databricks.spark.csv').save(edge_url)
