from sets import Set
import sys, os, itertools
import numpy as np

# Spark imports
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField
from pyspark.sql.types import StringType, LongType, ArrayType, DoubleType
import pyspark.sql.functions as sqlf

if os.path.isdir(sys.argv[1]):
    node_url          = "file:{0}".format(os.path.abspath(sys.argv[1]))
    genre_index_url   = "{0}".format(os.path.abspath(sys.argv[2]))
    word_index_url    = "{0}".format(os.path.abspath(sys.argv[3]))
    genre_table_url   = "file:{0}".format(os.path.abspath(sys.argv[4]))
    genre_edge_list_url = "file:{0}".format(os.path.abspath(sys.argv[5]))
else:
    err = '''
    Please provide the following files:
    1. parquet file of nodes
    2. location to wrtie genre index
    3. locaiton to write word index
    4. locaiton to write genre table
    5. location to write genre edge list
    '''
    print(err)
    sys.exit(1)

# Create SPARK Session + Config
spark = SparkSession \
    .builder \
    .appName("SPARK GENRE GRAPH") \
    .getOrCreate()

# Schema we'll end up using to represent a node
node_schema = StructType(
    [
        StructField('id',        StringType(),             False),
        StructField('node_type', StringType(),             False),
        StructField('words',     ArrayType(StringType()),  True),
        StructField('counts',    ArrayType(LongType()),    True),
        StructField('genre',     StringType(),             True)
    ]
)

# Read in Node DataFrame
node_df = spark.read.parquet(node_url)
print ("Done reading in nodes!")

# Collect Index Information
song_df = node_df.select("*").where("node_type = 'song'")
word_df = node_df.select("*").where("node_type = 'word'")
genre_df = song_df.groupBy("genre").agg(sqlf.count("id"))

word_index = word_df.distinct().rdd.map(lambda x: x.id).collect()
genre_index = genre_df.distinct().rdd.map(lambda x: x.genre).collect()

# Broadcast for mappers
GENRES = spark.sparkContext.broadcast(genre_index)
GENRE_COUNT = spark.sparkContext.broadcast(float(len(genre_index)))

# Emits word records for the song's genre
def genre_mapper(row):
    for word in row.words:
        word_id = (word,)
        genre_list = [0] * len(GENRES.value)
        genre_list[GENRES.value.index(row.genre)] = 1
        yield word_id + tuple(genre_list)

# Normalizes the row
def normal_mapper(row):
    # Count total word occurences
    count = sum(row[1:])
    # Divide each genre count by total, row sums to 1
    n = np.array(row[1:], dtype=np.float)
    n = [float(x) for x in n/float(count)]
    # Compute mean absolute deviation
    # i.e. average deviation from perfectly distributed word
    mad = sum(abs(x - 1.0/GENRE_COUNT.value) for x in n) / GENRE_COUNT.value
    return (row[0], mad, count) + tuple(n)

# Schemas
id_field =     [StructField('word', StringType(), False)]
mad_field =    [StructField('mean_abs_dev', DoubleType(), False)]
count_field =  [StructField('count', LongType(), False)]
float_fields = [StructField(field_name, DoubleType(), False) for field_name in genre_index]
int_fields =   [StructField(field_name, LongType(), False) for field_name in genre_index]
int_schema =   StructType(id_field + int_fields)
float_schema = StructType(id_field + mad_field + count_field + float_fields)

# Map/Reduce phase:
# 1. Map song word lists to genre count records
# 2. Group on word records, sum columns
# 3. Map from int counts to normalized floats
genre_table = song_df.rdd.flatMap(genre_mapper).toDF(schema=int_schema)
genre_table = genre_table.groupBy("word").sum()
genre_table = genre_table.rdd.map(normal_mapper).toDF(schema=float_schema)

# Second Map/Reduce phase
# 1. Generate networkx edge list format

# Schema we'll end up using to represent an edge
edge_schema = StructType(
    [
        StructField('src',  StringType(),  False),
        StructField('dst',  StringType(),  False),
        StructField('weight', DoubleType(), False)
    ]
)

def edge_map (row):
    for idx, genre_weight in enumerate(row[3:]):
        if genre_weight > 0:
            edge = (row.word, GENRES.value[idx] , genre_weight)
            yield edge

genre_edge_list = genre_table.rdd.flatMap(edge_map).toDF(schema=edge_schema)
genre_edge_list.show()

# Print Results to File
out = open(word_index_url, 'w')
for i in range (len(word_index)):
    if i == (len(word_index) - 1):
        out.write("{0}".format(word_index[i]))
    else:
        out.write("{0},".format(word_index[i]))
out.close()

out = open(genre_index_url, 'w')
for i in range (len(genre_index)):
    if i == (len(genre_index) - 1):
        out.write("{0}".format(genre_index[i]))
    else:
        out.write("{0},".format(genre_index[i]))
out.close()

genre_table.coalesce(1).write.option("header", "true").csv(genre_table_url)
# genre_edge_list.coalesce(1).write.option("header", "true").csv(genre_edge_list_url)
