# -*- coding: utf-8 -*-
import networkx as nx
import numpy, sys
import numpy.linalg as la
import scipy.sparse.linalg as sla
import math

'''
Code: Lauren Fuller
PageRank Algorithm based off of the Power Interation Method in:
https://projecteuclid.org/download/pdf_1/euclid.im/1128530802
'''
def norm(m):
    n = 0
    for x in range(0, m.shape[0]):
        n = n + m[x,0]
    return n

# Compute PageRank using the Power Iteration Method
def _pagerank(P, v, c):
    # Intialize p
    p = v
    k = 0
    e = 0.001
    d = 1
    Pt = P.transpose()

    # Repeat until d < e
    while (d >= e):
        pk = c * (Pt * p)
        y = norm(p) - norm(pk)
        pk = pk + (y * v)
        d = norm(pk - p)
        k = k + 1
        p = pk

    return np.squeeze(np.asarray(p))

def pagerank(G):
    print(nx.number_of_nodes(G))
    print(nx.number_of_edges(G))

    c = 0.85 # alpha
    v = [] # teleportation vector

    A = nx.adjacency_matrix(G)

    # Calculate the Teleportation Vector
    v = [np.double(1.0/A.shape[0])] * A.shape[0]
    v = np.asmatrix(v)
    v = np.transpose(v)

    # Calculate Degree Matrix
    D = [1] * A.shape[0]
    D = np.asmatrix(D)
    D = np.transpose(D)
    D = A * D

    # Calculate P
    P = []
    for x in range (0, A.shape[0]):
        recip_degree = np.array([1.0/D[x]])
        recip_degree_m = scipy.sparse.csr_matrix(np.matrix(recip_degree))
        P.append(recip_degree_m.multiply(A.getrow(x)))

    P = scipy.sparse.vstack(P)
    return _pagerank(P, v, c)

'''
Eigenvector centrality is simply the leading eigenvector of the adjacency_matrix
Returns centrality vector
'''
def eigenvector_cent_scipy (G):
    A = nx.adjacency_matrix(G).asfptype()
    eigen_vals, eigen_vectors = sla.eigs(A, k=1, which="LM")
    k1 = eigen_vals[0].real
    v1 = list(map(lambda x: x.real, eigen_vectors[:,0]))
    return v1

'''
Eigenvector centrality, but using the iterative algorithm.
By default, iterates until convergence is better than 1/10^6
Returns centrality vector
'''
def eigenvector_cent_iterative (G, precision=(1.0/(10.0**6)), iter_max=float('inf')):
    A = nx.adjacency_matrix(G).asfptype()

    # Set up basic eigenvector algorithm
    x0 = numpy.array([0 for x in range(0,A.shape[0])], dtype=float)
    x1 = numpy.array([1 for x in range(0,A.shape[0])], dtype=float)

    iteration = 0
    while la.norm(numpy.subtract(x0,x1)) > float(precision) and iteration < iter_max:
        x0 = x1
        x1 = A * x1
        x1 = x1/la.norm(x1)
        iteration += 1
        pass
    print "Converged to precision {0} in {1} iterations".format(precision, iteration)
    return x1

'''
Katz centrality, where we first need to find the leading eigenvalue to
choose an appropriate value for alpha. Beta set to 1, by default will
iterate until convergence is better than 1/10^6
Returns centrality vector
'''
def katz_cent (G, precision=(1.0/(10.0**6)), iter_max=float('inf')):
    A = nx.adjacency_matrix(G).asfptype()
    eigen_vals, eigen_vectors = sla.eigs(A, k=1, which="LM")

    #  Set up Katz algorithm, with alpha = 90% 1/k1 and beta = 1
    k1 = eigen_vals[0].real
    alpha = (1.0/k1)*0.9     # alpha must be less than 1/k1
    ONE =     numpy.array([1 for x in range(0,A.shape[0])],dtype=float)
    x0 = x1 = numpy.array([0 for x in range(0,A.shape[0])],dtype=float)

    iteration = 0;
    while True:
        x0 = x1
        x1 = (alpha * A * x1) + ONE
        x1 = x1/la.norm(x1)                # normalize
        iteration += 1
        if la.norm(numpy.subtract(x0,x1)) < precision or iteration > iter_max:
            break
        pass
    print "Converged to precision {0} in {1} iterations".format(precision, iteration)
    return x1

'''
HITS algorithm. Since A*At is symmetric, as is At*A, we can take the leading
eigenvectors as the centrality vectors. This principle vector will have all real values.
Returns pair of centrality vectors (x,y) for authorities and hubs
'''
def hits_cent_scipy(G):
    A = nx.adjacency_matrix(G).asfptype()
    At = A.transpose()
    auth_eigen_vals, auth_eigen_vectors = sla.eigs(At.multiply(A), k=1, which="LM")
    hub_eigen_vals, hub_eigen_vectors = sla.eigs(A.multiply(At), k=1, which="LM")
    x = list(map(lambda x: x.real, auth_eigen_vectors[:,0]))
    y = list(map(lambda x: x.real, hub_eigen_vectors[:,0]))
    return (x,y)

'''
HITS algorithm. Iterative algorithm as described by Kleinberg
Returns pair of centrality vectors (x,y) for authorities and hubs
'''
def hits_cent_iterative(G, precision=(1.0/(10.0**6)), iter_max=float('inf')):
    A = nx.adjacency_matrix(G).asfptype()
    At = A.transpose()

    # Set up HITS
    x0 = x1 = numpy.array([1 for x in range(0,A.shape[0])],dtype=float)
    y0 = y1 = numpy.array([1 for x in range(0,A.shape[0])],dtype=float)

    iteration = 0;
    while True:
        x0 = x1
        y0 = y1
        x1 = (At.multiply(A)) * y1    # I operation
        y1 = (A.multiply(At)) * x1    # O operation
        x1 = x1/la.norm(x1)         #
        y1 = y1/la.norm(y1)         # normalize
        iteration += 1
        if (
            (la.norm(numpy.subtract(x0,x1)) < precision and
             la.norm(numpy.subtract(y0,y1)) < precision) or
             iteration > iter_max
        ):
            break
        pass
    print "Converged to precision {0} in {1} iterations".format(precision, iteration)
    return (x1,y1)


# Runs a suite of centrality measures and sets node attributes
def centrality_suite(G):
    print ("Katz Centrality - Iterative Refinement: Top 5")
    cent_vec = katz_cent(G)
    cents = {n:k for n,k in zip(G.nodes(), cent_vec)}
    nx.set_node_attributes(G, "katz", cents)

    top_50 = sorted(zip(G.nodes(), cent_vec), key=lambda x: x[1], reverse=True)[:50]
    for entry in top_50:
        print entry
    print ("")

    print ("Pagerank")
    print ("")
    pr = pagerank(G)
    prs = {n:p for n,p in zip(G.nodes(), pr)}
    nx.set_node_attributes(G, "pagerank", prs)

    top_50 = sorted(zip(G.nodes(), pr), key=lambda x: x[1], reverse=True)[:50]
    for entry in top_50:
        print entry
    print ("")

    print ("HITS Centrality - Iterative Refinement")
    print ("")
    auth_vec, hub_vec = hits_cent_iterative(G)
    top_20_auth = sorted(zip(auth_vec, G.nodes()), reverse=True)[:20]
    top_20_hubs = sorted(zip(hub_vec, G.nodes()), reverse=False)[:20]

    print ("Authorities")
    for entry in top_20_auth:
        print entry
    print ("")

    print ("Hubs")
    for entry in top_20_hubs:
        print entry
    print ("")
