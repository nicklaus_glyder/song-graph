import networkx as nx
import pandas
import pandas as pd
import matplotlib.pyplot as plt
from collections import OrderedDict

# Cluster by removing nodes
def hierarchical_clustering_nodes(G, weight='', resolution=10):
    nodes = G.nodes(data=True)
    nodes = filter(lambda n: weight in n[1], nodes)
    nodes = sorted(nodes, key=lambda (k,v): v[weight])
    total_nodes = len(nodes)
    cc_count = nx.number_connected_components(G)
    cc_dict = OrderedDict()

    # Remove all nodes
    for x in range(total_nodes):
        node = nodes[0][0]
        nodes.pop(0)
        G.remove_node(node)
        if x % resolution == 0 and x != 0:
            cc_current = nx.number_connected_components(G)
            if cc_current > cc_count:
                cc_count = cc_current
                cc_dict[(cc_count,x)] = list(nx.connected_component_subgraphs(G))
        pass
    return cc_dict

# Cluster by removing edges
def hierarchical_clustering_edges(G, weight='', resolution=100):
    edges = nx.get_edge_attributes(G, weight)
    edges = sorted(edges.iteritems(), key=lambda (k,v): (v,k))
    total_edges = len(edges)
    cc_count = nx.number_connected_components(G)
    cc_dict = OrderedDict()

    # Remove all edges
    for x in range(total_edges):
        edge = edges[0][0]
        edges.pop(0)
        G.remove_edge(edge[0], edge[1])
        if x % resolution == 0 and x != 0:
            cc_current = nx.number_connected_components(G)
            if cc_current > cc_count:
                cc_count = cc_current
                cc_dict[(cc_count, x)] = list(nx.connected_component_subgraphs(G))
        pass
    return cc_dict

# Run the edge clustering alg and plot giant component size
def gc_size_edge_removal(G):
    result = hierarchical_clustering_edges(G.copy(), weight='weight', resolution=1000)
    gc_size = {c : max(g.order() for g in v) for c,v in result.iteritems()}
    gc_size = {c: float(gc)/float(G.order()) for c,gc in gc_size.iteritems()}
    gc_size[1] = 1

    df = pd.Series(gc_size, name='GC_SIZE')
    df.plot()
    plt.xlabel('(# of Components, # Removed Edges)')
    plt.ylabel('Giant Comp. Size (%)')
    plt.suptitle('Edge Removal Clustering')
    plt.show()

# Run the node clustering alg and plot giant component size
def gc_size_node_removal(G):
    # Run hierarchical_clustering using the given property
    result = hierarchical_clustering_nodes(G.copy(), weight='mad', resolution=100)
    gc_size = {c : max(g.order() for g in v) for c,v in result.iteritems()}
    gc_size = {c : float(gc)/float(G.order()) for c,gc in gc_size.iteritems()}
    gc_size[1] = 1

    df1 = pd.Series(gc_size, name='GC_SIZE')
    df1.plot()
    plt.xlabel('(# Connected Components, # Removed Edges)')
    plt.ylabel('Giant Comp. Size (%)')
    plt.suptitle('Node Removal Clustering')
    plt.show()
