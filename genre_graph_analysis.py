import sys, os, itertools
sys.setrecursionlimit(10000)
import csv
import networkx as nx
import numpy as np
import scipy as scp
from scipy.cluster import hierarchy
from collections import OrderedDict
import matplotlib.pyplot as plt
from algorithms.centrality_algs import *
from algorithms.hierarchical_clustering import *

if os.path.isfile(sys.argv[1]):
    genre_url = "{0}".format(os.path.abspath(sys.argv[1]))
    genre_index_url = "{0}".format(os.path.abspath(sys.argv[2]))
    genre_table_url = "{0}".format(os.path.abspath(sys.argv[3]))
else:
    err = '''
    Please provide the following files:
    1. genre graph edge list
    2. csv of the genre index
    3. csv of the genre metrics
    '''
    print(err)
    sys.exit(1)

# Create networkX undirected graph
G = nx.read_weighted_edgelist(genre_url, \
    comments='#', delimiter=',', create_using=nx.Graph(), nodetype=str)

# Read genre index
with open(genre_index_url, 'rb') as index:
    genre_index = index.read().split(',')

# Open the metric table for reading
with open(genre_table_url, 'rb') as table:
    reader = csv.reader(table)
    mad_index = {row[0]:row[1] for row in reader}
    count_index = {row[0]:row[2] for row in reader}

# Identify genre label nodes
def node_type(node):
    if node in genre_index:
        return 'genre'
    else:
        return 'song'

# Set the type property so we can avoid removing genre nodes
types = {x:node_type(x) for x in G.nodes()}
nx.set_node_attributes(G, "type", types)

# Set the mean avg dev property to order node removals
nx.set_node_attributes(G, "mad", mad_index)
nx.set_node_attributes(G, "count", count_index)

# Basic Graph Info
print nx.info(G)

# Generate ordering of nodes from index
nodes = G.nodes()
node_index = {}
for i, node in enumerate(nodes):
    node_index[node] = i

# Generate hierarchical clusters based on genre affinity
# result_edges = hierarchical_clustering_edges(G.copy(), weight='weight', resolution=1000)
# result_nodes = hierarchical_clustering_nodes(G.copy(), weight='mad', resolution=100)

gc_size_edge_removal(G)
gc_size_node_removal(G)

sys.exit()

# Identify genre nodes in each cluster
def genre_map(G):
    genres = filter(lambda g: g in G, genre_index)
    return (G.order(), genres, G.nodes())

# Disregard single node clusters
def cluster_filter(clusters):
    return clusters[0] > 1

# Map # of clusters to non-singuler connected components
clustering_map = OrderedDict()
for cluster_level, subgraphs in result_edges.iteritems():
    clustering_map[cluster_level[0]] = \
        filter(cluster_filter, map(genre_map, subgraphs))

# Record cluster data for review
with open("clusters.txt",'w') as cluster_file:
    for c,v in clustering_map.iteritems():
        cluster_file.write(str("Number of clusters {0}:\n\n".format(c)))
        for cluster in v:
            cluster_file.write(str(cluster[0]) + " " + str(cluster[1]))
            cluster_file.write("\n")
        cluster_file.write("\n---------------------------\n\n")

'''
    Generate the dendrogram. We need an upper triangular distance
    matrix of least distance bewtween all nodes. In our case, the least
    distance between 2 nodes is the smallest cluster that they both appear in

    Start with all nodes in a single cluster. Identify which cluster each node
    belongs too at a given level, and update its row entries
'''
distance_matrix = []
total_nodes = len(nodes)
sorted_nodes = sorted(nodes, key=lambda n: node_index[n])

# Generate upper triangular row by row
for node in sorted_nodes:
    pos = node_index[node]         # Numerical ordering in matrix
    distance_vector = [total_nodes] * total_nodes
    for num_clusters, clusters in list(clustering_map.iteritems())[5:]:
        found_node = False
        for cluster in clusters:
            if node in cluster[2]:
                for mate in cluster[2]:
                    if node_index[mate] > pos:
                        distance_vector[node_index[mate]] = cluster[0]
                found_node = True
                continue
        if found_node == False:
            break
    distance_matrix.append(distance_vector)

# Compute dendrogram using single linkage
L = np.array(distance_matrix)[np.triu_indices(total_nodes,1)]
Z = hierarchy.linkage(L, 'single')

# Plot dendrogram with or without labels
font = {'size':18}
plt.rc('font', **font)
plt.figure()
dn = hierarchy.dendrogram(Z, no_labels=True, leaf_font_size=14, leaf_rotation=45)
plt.xlabel("Cluster Membership")
plt.ylabel("Cluster Size")
plt.suptitle("Removal Clustering")
plt.show()
